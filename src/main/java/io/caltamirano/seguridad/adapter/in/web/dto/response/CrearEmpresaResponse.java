package io.caltamirano.seguridad.adapter.in.web.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@SuppressWarnings("all")
public class CrearEmpresaResponse {

	private String id;
	
	private String ruc;
	
	private String razonSocial;
	
}
